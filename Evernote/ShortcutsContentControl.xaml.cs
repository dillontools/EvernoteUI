﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Evernote
{
    /// <summary>
    /// Interaction logic for ShortcutsControl.xaml
    /// </summary>
    public partial class ShortcutsContentControl : UserControl
    {
        public ShortcutsContentControl()
        {
            InitializeComponent();
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.listView.SelectedItem == null)
            {
                return;
            }
            var selectedItem = this.listView.SelectedItem as IContentKeyProvider;
            if (selectedItem != null)
            {
                App.ContextSelectionService.SetSelectedContext(selectedItem);
            }
        }
    }
}
