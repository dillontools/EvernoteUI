﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Evernote
{
    public class TreeViewMode : IContentKeyProvider
    {
        public TreeViewMode()
        {
            this.HasText2 = false;
        }
        public string Image { get; set; }
        public string Text { get; set; }
        public string Tooltip { get; set; }
        public string NoteType { get; set; }
        string text2;
        public string Text2
        {
            get
            {
                return this.text2;
            }
            set
            {
                if (this.text2 == value)
                {
                    return;
                }
                this.text2 = value;
                this.HasText2 = !string.IsNullOrEmpty(this.text2);
            }
        }
        public bool HasText2 { get; set; }
        TreeViewModeCollection children;
        public TreeViewModeCollection Children
        {
            get
            {
                if (this.children == null)
                {
                    this.children = new TreeViewModeCollection();
                }
                return this.children;
            }
            set
            {
                this.children = value;
            }
        }
        public bool IsExpanded { get; set; }
        public bool IsSelected { get; set; }

        public string ContentKey1 { get; set; }
        public string ContentKey2 { get; set; }
    }

    public class TreeViewModeCollection :
        System.Collections.ObjectModel.ObservableCollection<TreeViewMode>
    {
        //public bool IsExpanded { get; set; }
        //public bool IsSelected { get; set; }

    }
}
